FROM alpine:latest

ARG INTERVAL
ENV INTERVAL=${INTERVAL}
ENV TZ=Europe/Istanbul

COPY birthday.py /birthday.py
COPY birthday.txt /birthday.txt

RUN apk update && apk add --no-cache tzdata py3-pip nano \
    && echo "${INTERVAL} * * * /usr/bin/python3 /birthday.py" > /var/spool/cron/crontabs/root

CMD ["/usr/sbin/crond", "-f", "-d", "0"]
