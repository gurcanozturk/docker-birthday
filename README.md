# docker-birthday

Automate your birthday messages with docker.

## Prepare your message
Put message what you want to send into birthday-message.txt

## Prepare your birthday db file.
Put your data into birthday.txt formatted as below (date format should DD/MM/YYYY)

Chuck Norris:chuck@norris.com:10/03/1940:M

## Build it. 
(INTERVAL = "Crontab style date. Sample will work everday at 00:01)

docker build -t birthdayer --build-arg INTERVAL="01 00" .

## Run it.
docker run --rm --name birthdayer -e DEBUG=1 -e MAIL_FROM=<your_email> -e SMTP_SERVER=<your_smtp_server> birthday