#!/usr/bin/python3

from datetime import date
import os
import time
import logging
import smtplib
from email.mime.text import MIMEText

def send_mail(mail_from, mail_to, name):
  with open("/birthday-message.txt", 'r') as fp:
    # Create a text/plain message
    msg = MIMEText(fp.read())

  msg['Subject'] = 'Happy birthday %s' % name
  msg['From'] = mail_from
  msg['To'] = mail_to

  s = smtplib.SMTP(SMTP_SERVER)
  s.sendmail(mail_from, mail_to, msg.as_string())
  s.quit()

DEBUG = os.environ.get('DEBUG')
MAIL_FROM = os.environ.get('MAIL_FROM')
SMTP_SERVER = os.environ.get('SMTP_SERVER')

logging.basicConfig(filename="/var/log/birthday.log",
                    filemode='a',
                    format='%(asctime)s,%(msecs)d %(name)s %(levelname)s %(message)s',
                    datefmt='%H:%M:%S',
                    level=logging.DEBUG)

today = date.today().strftime("%d/%m")
year  = date.today().strftime("%Y")

persons = open("/birthday.txt",'r')

for line in persons:
    line = line.strip()
    name  = line.split(':')[0]
    email = line.split(':')[1]
    bday  = line.split(':')[2]
    gender = line.split(':')[3]

    if bday[:-5] == today:
      age = (int(year) - int(bday[6:]))
      msg = ("today is %s(%s)'s birthday, let's send email to %s" % (name, age, email))

      if SMTP_SERVER and MAIL_FROM and email and name:
        send_mail(MAIL_FROM, email, name)

persons.close()
